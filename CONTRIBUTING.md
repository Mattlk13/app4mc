# Contributing to Eclipse APP4MC

Thanks for your interest in this project.

## Project description

Application Platform Project for Multi Core

* https://projects.eclipse.org/projects/automotive.app4mc

## Developer resources

Information regarding source code management, builds, coding standards, and
more.

* https://projects.eclipse.org/projects/automotive.app4mc/developer

The project maintains the following source code repositories

* https://git.eclipse.org/r/plugins/gitiles/app4mc/org.eclipse.app4mc
* https://git.eclipse.org/r/plugins/gitiles/app4mc/org.eclipse.app4mc.addon.migration
* https://git.eclipse.org/r/plugins/gitiles/app4mc/org.eclipse.app4mc.addon.transformation
* https://git.eclipse.org/r/plugins/gitiles/app4mc/org.eclipse.app4mc.cloud
* https://git.eclipse.org/r/plugins/gitiles/app4mc/org.eclipse.app4mc.tools
* https://git.eclipse.org/r/plugins/gitiles/app4mc/org.eclipse.app4mc.examples

This project uses Bugzilla to track ongoing development and issues.

* Search for issues: https://bugs.eclipse.org/bugs/buglist.cgi?product=APP4MC
* Create a new report:
   https://bugs.eclipse.org/bugs/enter_bug.cgi?product=APP4MC

Be sure to search for existing bugs before you create another one. Remember that
contributions are always welcome!

## Eclipse Development Process

This Eclipse Foundation open project is governed by the Eclipse Foundation
Development Process and operates under the terms of the Eclipse IP Policy.

* https://eclipse.org/projects/dev_process
* https://www.eclipse.org/org/documents/Eclipse_IP_Policy.pdf

## Eclipse Contributor Agreement

In order to be able to contribute to Eclipse Foundation projects you must
electronically sign the Eclipse Contributor Agreement (ECA).

* http://www.eclipse.org/legal/ECA.php

The ECA provides the Eclipse Foundation with a permanent record that you agree
that each of your contributions will comply with the commitments documented in
the Developer Certificate of Origin (DCO). Having an ECA on file associated with
the email address matching the "Author" field of your contribution's Git commits
fulfills the DCO's requirement that you sign-off on your contributions.

For more information, please see the Eclipse Committer Handbook:
https://www.eclipse.org/projects/handbook/#resources-commit

## Contact

Contact the project developers via the project's "dev" list.

* https://dev.eclipse.org/mailman/listinfo/app4mc-dev
