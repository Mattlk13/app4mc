---
date: 2022-11-30
title: "Downloads"
weight: 5
hide_sidebar: "true"
---


---
### **Release 3.0.0**
##### 2022-11-30 &nbsp;&nbsp; - &nbsp;&nbsp; based on Eclipse 2020-06
---

Recommended Java runtime is **Java 11**. Minimum is Java 8 (with limitations if JavaFX is not included).

#### Products

* APP4MC Platform 3.0.0 for **Linux (64 Bit)** - [Zip-archive](https://www.eclipse.org/downloads/download.php?file=/app4mc/products/releases/3.0.0/org.eclipse.app4mc.platform-3.0.0-20221130-125547-linux.gtk.x86_64.tar.gz) (400 MB)

* APP4MC Platform 3.0.0 for **OSX (64 Bit)** - [Zip-archive](https://www.eclipse.org/downloads/download.php?file=/app4mc/products/releases/3.0.0/org.eclipse.app4mc.platform-3.0.0-20221130-125547-macosx.cocoa.x86_64.dmg) (400 MB)

* APP4MC Platform 3.0.0 for **Windows (64 Bit)** - [Zip-archive](https://www.eclipse.org/downloads/download.php?file=/app4mc/products/releases/3.0.0/org.eclipse.app4mc.platform-3.0.0-20221130-125547-win32.win32.x86_64.zip) (400 MB)

#### Update Site

* direct link: https://download.eclipse.org/app4mc/updatesites/releases/3.0.0/

* for offline installation: [Zip-archive of APP4MC 3.0.0 p2 repository](https://www.eclipse.org/downloads/download.php?file=/app4mc/products/releases/3.0.0/org.eclipse.app4mc.p2repo-3.0.0.zip)

---

_For older versions please refer to:_

* [Eclipse APP4MC project page](https://projects.eclipse.org/projects/automotive.app4mc/downloads)
