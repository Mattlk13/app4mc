---
title: "Documentation"
date: 2022-12-02
hide_sidebar: "false"
---


Sign up on the [APP4MC mailing list](https://dev.eclipse.org/mailman/listinfo/app4mc-dev) for the latest news about APP4MC.

---
### Help
---

#### **APP4MC**

<a href="/help/latest/index.html" target="_blank">APP4MC 3.0.0 - Online Help</a> (opens in new tab)

[APP4MC 3.0.0 - Help](https://archive.eclipse.org/app4mc/documents/help/app4mc-3.0.0-help.zip) (zip archive)

<br>
#### Previous versions (zip archive only):

[2.2.0](https://archive.eclipse.org/app4mc/documents/help/app4mc-2.2.0-help.zip) - 
[2.1.0](https://archive.eclipse.org/app4mc/documents/help/app4mc-2.1.0-help.zip) - 
[2.0.0](https://archive.eclipse.org/app4mc/documents/help/app4mc-2.0.0-help.zip) - 
[1.2.0](https://archive.eclipse.org/app4mc/documents/help/app4mc-1.2.0-help.zip) - 
[1.1.0](https://archive.eclipse.org/app4mc/documents/help/app4mc-1.1.0-help.zip) - 
[1.0.0](https://archive.eclipse.org/app4mc/documents/help/app4mc-1.0.0-help.zip) - 
[0.9.9](https://archive.eclipse.org/app4mc/documents/help/app4mc-0.9.9-help.zip) - 
[0.9.8](https://archive.eclipse.org/app4mc/documents/help/app4mc-0.9.8-help.zip) - 
[0.9.7](https://archive.eclipse.org/app4mc/documents/help/app4mc-0.9.7-help.zip) - 
[0.9.6](https://archive.eclipse.org/app4mc/documents/help/app4mc-0.9.6-help.zip) - 
[0.9.5](https://archive.eclipse.org/app4mc/documents/help/app4mc-0.9.5-help.zip) - 
[0.9.4](https://archive.eclipse.org/app4mc/documents/help/app4mc-0.9.4-help.zip) - 
[0.9.3](https://archive.eclipse.org/app4mc/documents/help/app4mc-0.9.3-help.zip) - 
[0.9.2](https://archive.eclipse.org/app4mc/documents/help/app4mc-0.9.2-help.zip) - 
[0.9.1](https://archive.eclipse.org/app4mc/documents/help/app4mc-0.9.1-help.zip) - 
[0.9.0](https://archive.eclipse.org/app4mc/documents/help/app4mc-0.9.0-help.zip) - 
[0.8.3](https://archive.eclipse.org/app4mc/documents/help/app4mc-0.8.3-help.zip) - 
[0.8.2](https://archive.eclipse.org/app4mc/documents/help/app4mc-0.8.2-help.zip) - 
[0.8.1](https://archive.eclipse.org/app4mc/documents/help/app4mc-0.8.1-help.zip) - 
[0.8.0](https://archive.eclipse.org/app4mc/documents/help/app4mc-0.8.0-help.zip) - 
[0.7.2](https://archive.eclipse.org/app4mc/documents/help/app4mc-0.7.2-help.zip)  

---
#### **APP4MC.sim**

[APP4MC.sim - Online Help](../doc-app4mc.sim)

<br>

---
### Related Standards
---

#### **BTF Specification**
Current version: [V2.2.1 (2021-01-07)](https://archive.eclipse.org/app4mc/documents/misc/BTF_Specification_2.2.1.pdf)

Previous versions:
[V2.1.5 (2016-01-29)](https://archive.eclipse.org/app4mc/documents/misc/BTF_Specification_2.1.5.pdf)
