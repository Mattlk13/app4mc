---
date: 2022-07-12
title: "Documentation APP4MC.sim"
hide_sidebar: "true"
---

## Overview

APP4MC.sim is a simulation framework for effective and early analysis of the runtime behavior of AMALTHEA models. It allows the consideration of software aspects and also the investigation of effects caused by concurrent accesses to shared hardware resources, especially memory in multi-core architectures. At its core, APP4MC.sim is based on the SystemC library with TLM extensions, which provides a solid foundation due to its widespread use and large user community.

This page provides an overview and step-by-step guide for the timing simulation of AMALTHEA models using APP4MC.sim.

### APP4MC.sim Flow

The image below provides an overview on the procedure for timing simulation of an AMALTHEA model using APP4MC.sim. This procedure consists of two steps, namely, **transformation** and **simulation**, which are summarized in the following.

The (model-to-text) **transformation** step takes as input an AMALTHEA model and generates APP4MC.sim-specific code based on that model. The generated code, also called **simulation model**, consists of (i) C++ source files that describe the model and the timing simulation setup using APP4MC.sim and SystemC APIs and (ii) a set of scripts (e.g., CMake and batch) that are required for configuring, building, and running the simulation.


The **simulation** step is carried out by the APP4MC.sim simulator. For that, first, the simulation model generated in the transformation step and the source code of APP4MC.sim are used to build the simulation project. After that, the simulation can be performed which generates one or multiple trace files (in .vcd and .btf formats).  
&nbsp;
<div style="text-align: center"><img src="/images/doc-app4mc.sim/app4mcsim-flow.png" class="img-responsive"> </div>

---
## Prerequisites

1. Java 11 SDK. The JAVA_HOME variable must be set to point to the java installation directory.
2. Git (required at command line / command prompt)

---

## Transformation Guide

This part provides a step-by-step guide on the tool setup and the transformation procedure to generate an APP4MC.sim simulation model for a given AMALTHEA model.

### Tool Setup

1. Download APP4MC available at https://projects.eclipse.org/projects/automotive.app4mc (The APP4MC.sim simulation currently supports version 2.0.0).
2. Launch APP4MC
    1. Unzip the downloaded package and launch "app4mc.exe"
    2. Specify a workspace directory (or use the default directory suggested) when prompted
    3. Close the welcome page
3. Add the required P2 Update sites
    1. From the **Help** menu, select the item **Install New Software** to open the **Install** window  
    <div style="text-align: center"><img src="/images/doc-app4mc.sim/install-new-software.png" class="img-responsive"> </div>
    2. On the opened dialog, click **Manage...**
    3. On the new dialog, enable the **APP4MC Model Transformation P2 Updatesite** and the **APP4MC AMALTHEA to SystemC transformation P2 Updatesite** (see the image below).  
    **Note:** If the updatesites are not available in the list, add them using the **Add...** button and then check the box next to the newly added items.
        * APP4MC Model Transformation P2 Updatesite:  
        https://download.eclipse.org/app4mc/components/addon/transformation/snapshot/p2repo
        * APP4MC AMALTHEA to SystemC transformation P2 Updatesite:  
        https://download.eclipse.org/app4mc/components/addon/transformation.sim.app4mc/snapshot/p2repo
    4. Click **Apply and Close**
    <div style="text-align: center"><img src="/images/doc-app4mc.sim/adding-update-sites.png" class="img-responsive"> </div>

4. Install the transformation framework
    1. In the **Install** window, under **Work with:** drop-down menu, select **APP4MC Model Transformation P2 Updatesite** (snapshot below)
    2. Check box for the item **APP4MC Model Transformation Framework** and the item **APP4MC Model Transformation UI Feature**
    3. Click **Next > Next > Finish**  
    **Note:** The installation may take a few seconds (progress state is shown in the APP4MC status bar at the lower-right corner of APP4MC)
    4. After the installation is complete, you will be prompted to restart the APP4MC platform. Click **Restart Now** on the opened prompt to proceed with the restart  
    <div style="text-align: center"><img src="/images/doc-app4mc.sim/install-transformation.png" class="img-responsive"> </div>


5. Install the AMALTHEA-to-SystemC transformation framework
    1. In the **Install** window, under **Work with:** drop-down menu, select **APP4MC AMALTHEA to SystemC transformation P2 Updatesite** (snapshot below)
    2. Check box for the item **Amalthea to SystemC transformation**
    3. Click **Next > Next > Finish**  
    **Note:** The installation may take a few seconds (progress state is shown in the APP4MC status bar at the lower-right corner of APP4MC)
    4. After the installation is complete, you will be prompted to restart the APP4MC platform. Click **Restart Now** on the opened prompt to proceed with the restart  
    <div style="text-align: center"><img src="/images/doc-app4mc.sim/install-transformation-sim.png" class="img-responsive"> </div>

6. Clone the APP4MC.sim repository (recursively with the submodules) from the [APP4MC.sim Eclipse Repository](https://gitlab.eclipse.org/eclipse/app4mc/org.eclipse.app4mc.tools.simulation.git). Then, update the repository and switch to the version compatible with APP4MC 2.0.0 as follows
    1. `git fetch --all --tags`
    2. `git checkout tags/APP4MC_v2.0.0`

### Model Transformation

The following describes the procedure of transforming an AMALTHEA model to generate its corresponding simulation model for timing simulation using APP4MC.sim.

**Note:** The page [Getting Started with APP4MC](../gs-create-model) provides details on the structure, contents, and the creation of an AMLTHEA model.

1. Open AMALTHEA model in the standard AMALTHEA tree editor and validate it for APP4MC.sim using the **APP4MC.sim Validations** as shown in the following image.  
<div style="text-align: center"><img src="/images/doc-app4mc.sim/sim-validations.png" class="img-responsive"> </div>

2. In the Project Explorer, under the context menu of the model, select **Amalthea > Transformation**   <br>
**Note:** The transformation option will not be available if your model is at an older version of AMALTHEA compared to the APP4MC platform. To solve the issue, migrate the model to the platform's version as follows:
    1. In the Project Explorer, under the context menu of the model, select **Amalthea > Model Migration**
    2. Click on **Migrate Model** and then **OK**
    3. In the Project Explorer, under the context menu of the migrated model, select **Amalthea > Transformation**  
    <div style="text-align: center"><img src="/images/doc-app4mc.sim/tranformation-context.png" class="img-responsive"> </div>

3. In the opened AMALTHEA transformation dialog, provide the information required for the transformation
    1. **Output Folder**: Optionally modify the output folder (this is where the simulation model generated during the transformation will be stored)
    2. **APP4MC.sim folder**: Provide the path to the root folder of the APP4MC.sim clone on your system
    3. **Simulation duration \[ms\]**: Optionally provide the simulation duration (default set to 1000ms)
    4. **Trace folder**: Optionally provide the target directory for the traces generated during the simulation (default set to .\traces)
    5. **Model to Text Transformation**: Check the box for **Amalthea to APP4MC.sim project transformation**
    6. click **OK**  
    <div style="text-align: center"><img src="/images/doc-app4mc.sim/transformation-dialog.png" class="img-responsive"> </div>

4. Once the transformation is complete, the generated simulation model files will be available at the output folder specified in the previous step. The generated sub-directory "APP4MCSIM" contains the transformation outputs required for timing simulation using APP4MC.sim.
<div style="text-align: center"><img src="/images/doc-app4mc.sim/simulation-model-folder.png" class="img-responsive"> </div>
<br>
**Note:** If the output folder is not shown in the Project Explorer, refresh the parent directory of the target output folder from its context menu.  
<div style="text-align: center"><img src="/images/doc-app4mc.sim/simulation-model-refresh.png" class="img-responsive"> </div>

---
## Simulation Guide

This part provides a step-by-step guide on the tool setup and the procedure to perform (in Windows) the timing simulation using APP4MC.sim for simulation models generated through the transformation procedure detailed above.

### Tool Setup
1. Download the "Visual Studio Build Tools 2019" available at https://visualstudio.microsoft.com/de/downloads/
2. Once downloaded, launch the installer "vs_BuildTools.exe" and install "C++ build tools" or alternatively, called "Desktop Development with C/C++" (requires admin rights)

### APP4MC.sim Timing Simulation
1. Build the simulation project (in Developer Command Prompt for VS 2019)
    1. Switch the working directory to the transformation output sub-directory "APP4MCSIM"
    2. Configure the project using the command `build.bat --configure`  
    <div style="text-align: center"><img src="/images/doc-app4mc.sim/sim-configure.png" class="img-responsive"> </div>
    3. Once the configuration has finished, build the project using the command `build.bat --build`  
    <div style="text-align: center"><img src="/images/doc-app4mc.sim/sim-build.png" class="img-responsive"> </div>

2. Run the simulation
    1. Run the simulation using the command `run.bat`  
    <div style="text-align: center"><img src="/images/doc-app4mc.sim/sim-run.png" class="img-responsive"> </div>
    2. Optionally, you can change the simulation duration and/or the trace directory at this point by directly running the command highlighted in the snapshot above with modified values passed to arguments `-t` (simulation duration in ms) and `-o` (output trace directory)
    3. After the simulation has finished, the generated trace file is available in the trace output directory 
    <div style="text-align: center"><img src="/images/doc-app4mc.sim/sim-trace.png" class="img-responsive"> </div>