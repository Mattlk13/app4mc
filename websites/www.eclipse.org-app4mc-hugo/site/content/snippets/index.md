---
date: 2020-05-01
menu:
  main:
    parent: "About"
title: Snippets
weight: 7
hide_sidebar: "true"
---

---
### Java
---

{{< highlight java "linenos=table,linenostart=1" >}}
/**
 * Basic validations for AMALTHEA
 */
 
@ValidationGroup(
	severity = Severity.ERROR,
	validations =  {
		AmBasicQuantity.class,
		AmBasicTimeRange.class
	}
)

@ValidationGroup(
	severity = Severity.WARNING,
	validations =  {
		AmBasicCustomPropertyKey.class
	}
)

public class BasicProfile implements IProfile {
    // Do nothing
}
{{< /highlight >}}
