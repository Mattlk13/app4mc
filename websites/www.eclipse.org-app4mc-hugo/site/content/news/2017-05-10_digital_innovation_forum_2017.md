---
date: 2017-05-08
title: Visit APP4MC/AMALTHEA4public at Digital Innovation Forum in Amsterdam
slug: "2017-05-10_digital_innovation_forum_2017"
categories: ["news"]
---

On May, the 10 - 11th, APP4MC will be present at the exhibition of the Digital Innovation Forum in Amsterdam. We'll present our latest platform developments and prototypes as well as some practical demonstrations developed with the APP4MC platform.

<!--more-->

You can visit us at booth 8 on the exhibition floor
[Digital Innovation Forum](https://dif2017.org/exhibition.html).

Demonstrators:

- SW distribution based on the Polarsys Rover chasis: (https://wiki.eclipse.org/APP4MC/Rover)

- EBEAS - Emergency Braking & Evasion Assistance System:
Traceability throughout the engineering phases shown with an open source ADAS example


<img src="/images/news/ebeas_designflow_small.png" title="EBEAS Overview" class="img-responsive">
