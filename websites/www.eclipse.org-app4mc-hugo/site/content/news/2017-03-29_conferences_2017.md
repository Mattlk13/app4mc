---
date: 2017-03-29
title: Visit APP4MC at DATE or Parallel conferences
slug: "2017-03-29_conferences_2017"
categories: ["news"]
---

On March, the 30th, the talk 'On Reducing Busy Waiting in AUTOSAR via Task-Release-Delta-based Runnable Reordering' (2pm) presents analyses regarding resource management based upon APP4MC at the Design, Automation and Test Conference and Exhibition (DATE).

<!--more-->

On March, the 30th, the talk 'On Reducing Busy Waiting in AUTOSAR via Task-Release-Delta-based Runnable Reordering' (2pm) presents analyses regarding resource management based upon APP4MC at the [Design, Automation and Test Conference and Exhibition (DATE)](https://www.date-conference.com/conference/session/11.5).

Furthermore, on Friday, March the 31st 10:40am, you can learn how to use APP4MC to analyize and optimize embedded multi-core systems and their performance at [parallel2017](https://www.parallelcon.de/veranstaltung-5701-performanceanalyse-und--optimierung-von-embedded-multicore-systemen.html?id=5701).


<!--more-->

__Abstract DATE__

Below, please find the talk's corresponding paper abstract for the DATE conference.

The increasing amount of innovative software technologies in the automotive domain comes with challenges regarding inevitable distributed multi-core and many-core methodologies. Approaches for general purpose solutions have been studied over decades but do not completely meet the specific constraints (e.g. timing, safety, reliability, affinity, etc.) for AUTOSAR compliant applications. AUTOSAR utilizes a spinlock mechanism in combination with the priority ceiling protocol in order to provide mutually exclusive access to shared resources. The essential disadvantages of spinlocks are unpredictable task response times on the one hand and wasted computation time caused by busy waiting periods on the other hand. In this paper, we propose a concept of task-release-delta-based runnable reordering for the purpose of sequentializing parallel accesses to shared resources, resulting in reduced task response times, improved timing predictability, and increased parallel efficiency respectively. To achieve this, runnables that represent smallest executable program parts in AUTOSAR are reordered based on precedence constraints. Our experiments among industrial use cases show that task response times can be reduced by up to 18,2%.

__Abstract parallel__

Based on the APP4MC Eclipse platform, we present the steps required in the development process to make the best use of Multicore controller's resources. Depending on the development status and the need for analyses, different levels of detail are needed to make targeted evaluations. Examples show which data is required at which development process and what tools can be utilized to extract this data from different sources. The analyzes range from forecast generation in early development phases to the detailed distribution and optimization of the software artifacts to existing hardware.
