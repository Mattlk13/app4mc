---
date: 2018-02-13
title: Workshop on Software Engineering for Applied Embedded R-T Systems
slug: "2018-02-13_seerts_workshop_ulm"
categories: ["news"]
---

Join the APP4MC team at the SEERTS workshop on March 5, 2018, Ulm University, held in conjunction with SE 18. 

<!--more-->

Join the APP4MC team at the [SEERTS workshop](https://gitlab-pages.idial.institute/robert.hoettger/SEERTS_workshop/Program.html) on March 5, 2018, Ulm University, held in conjunction with [SE 18](https://se18.uni-ulm.de/). 

Workshop participants will have the opportunity to learn and exchange on topics including parallelization strategies, scheduling analysis, tracing methods, and simulation. Various use cases as well as holistic, hypothetical, and industrial examples along with IDEs such as [Eclipse APP4MC](https://www.eclipse.org/app4mc/) or [Eclipse Kuksa](https://projects.eclipse.org/projects/iot.kuksa) give insights into state-of-the-art methodologies and tools for e.g. Automotive IoT projects.
