---
date: 2018-11-06
title: "APP4MC - Release 0.9.2 published"
categories: ["news"]
slug: "2018-11-06-release-0-9-2"
---

We released a new version of APP4MC with a couple of new features and improvements.

<!--more-->

__Extract of APP4MC 0.9.2__

Model handling

    Model migration support (0.9.1 -> 0.9.2)
	New model search dialog
	New model builders (with a Groovy-like builder pattern)

Model changes / Improvements

    Small extensions in the hardware model

* Visit the [APP4MC download page](https://projects.eclipse.org/projects/automotive.app4mc/downloads)
