---
date: 2016-06-27
title: Summer School Automotive 2016
slug: "2016-06-27_summer_school_automotive_2016"
categories: ["news"]
---

Summer School at the University of Applied Sciences and Arts in Dortmund from the 27th of June until the 1st of July 2016

<!--more-->

More about the [Summer School](https://internationalresearchdortmund.wikispaces.com/Automotive2016)

## Automotive Software Engineering

### Target: develop a software system for a multicore microcontroller with the AMALTHEA tool chain

__Trainers:__  

* Lukas Krawczyk, Uwe Lauschner, Robert Hoettger, Phil Naerdemann (Dortmund University of Applied Sciences and Arts)
* Jan Meel (KU Leuven)
* Andreas Sailer (Timing-Architects Embedded Systems GmbH)
* Lothar Wendehals (itemis AG)
* Mikael Barbero (Eclipse Foundation Europe)
* Markus Schmidt (Behr-Hella Thermocontrol GmbH)
