---
date: 2016-07-05
title: FMTV Challenge 2016
slug: "2016-07-05_fmtv_challenge"
categories: ["news"]
---

The Formal Methods for Timing Verification challenge (FMTV)
at International Workshop on Analysis Tools and Methodologies
for Embedded and Real-time Systems (WATERS)
is based on an Amalthea model of a Bosch Engine Control.

<!--more-->

The Formal Methods for Timing Verification challenge
([FMTV](https://waters2016.inria.fr/challenge/))
at International Workshop on Analysis Tools and Methodologies
for Embedded and Real-time Systems
([WATERS](http://waters2016.inria.fr/))
is based on an Amalthea model of a Bosch Engine Control.

__About the FMTV Challenge__

The purpose of the Formal Methods for Timing Verification (FMTV) challenge is to share ideas, experiences and solutions to a concrete timing verification problem issued from real industrial case studies. It also aims at promoting discussions, closer interactions, cross fertilization of ideas and synergies across the breadth of the real-time research community, as well as attracting industrial practitioners from different domains having a specific interest in timing verification.

__The 2016 FMTV Challenge__

The 2016 challenge is proposed by Arne Hamann, Simon Kramer, Martin Lukasiewycz and Dirk Ziegenbein from Robert Bosch GmbH.

An initial version of the challenge is available here:
[FMTV_challenge_2016_Bosch.pdf](http://ecrts.eit.uni-kl.de/forum/download/file.php?id=35)

The model can be downloaded here:
[FMTV_Challenge_2016_Bosch_Engine_Control_Model.zip](http://ecrts.eit.uni-kl.de/forum/download/file.php?id=36)