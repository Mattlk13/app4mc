---
date: 2020-12-01
title: "APP4MC - Release 1.0.0 published"
categories: ["news"]
slug: "2020-12-01-release-1-0-0"
---

We released a new version of APP4MC with a couple of new features and improvements.

<!--more-->

__Extract of APP4MC 1.0.0__


Model handling

	Model migration support (0.9.9 -> 1.0.0)

Model changes

	Removed MeasurementModel (deprecated since 0.9.9 and replaced by new ATDB)
	Removed ProcessEventType 'deadline'

Product

	New Amalthea Trace Database (ATDB)
	New transformations BTF -> ATDB, ATDB -> Amalthea
	Extended Metrics Viewer
	Several bug fixes

UI

	Added possibility to refresh a pinned visualization


__Further details__

* See slides [New & Noteworthy](https://archive.eclipse.org/app4mc/documents/news/APP4MC_1.0.0_New_and_noteworthy.pdf)
* Visit the [APP4MC download page](https://projects.eclipse.org/projects/automotive.app4mc/downloads)
