---
title: "Eclipse APP4MC"
seo_title: "Eclipse APP4MC"
headline: "Eclipse APP4MC"
date: 2018-04-05T15:50:25-04:00
hide_sidebar: "false"
---
<br>

 **Eclipse APP4MC** is a platform for engineering embedded multi- and many-core software systems. The platform enables the creation and management of complex tool chains including simulation and validation. As an open platform, proven in the automotive sector by Bosch and their partners, it supports interoperability and extensibility and unifies data exchange in cross-organizational projects.

<img src="images/system-model.png" class="img-responsive">


***Multi- and Many-Core Development Process Support***

The Amalthea platform allows users to distribute data and tasks to the target hardware platforms, with the focus on optimization of timing and scheduling. It addresses the need for new tools and techniques to make effective use of the level of parallelism in this environment.

***Common Data Exchange and Simulation***

The System Model contains the information required to simulate, analyze and optimize performance. It contains extensive information about software, hardware, timing behavior, and constraints for the system under development.

Based on the [Eclipse Modeling Framework](http://www.eclipse.org/modeling/emf/), its capabilities not only include hardware and software modelling but in addition, tools for visualization and processing. The unified data model enables tool interoperability and data exchange with other systems such as [Autosar](http://www.autosar.org) and simplifies the exchange and storage of data.

***Event Tracing***

The AMALTHEA definitions of task states and transitions provides the basis for evaluating systems with tracing tools, identifying problems and discovering improvements. These definitions are already in use in both commercial and open source tools.

***Customizable Workflow***

The AMALTHEA workflow engine supports a continuous development workflow, allowing data exchange along the entire toolchain. Workflow elements can be customized and extended according to user needs. As a basis, a standard workflow developed with [Eclipse Xtext(MWE2)](http://www.eclipse.org/Xtext/documentation/306_mwe2.html) is included.

<h2>What is APP4MC?</h2>

Get a quick overview of how APP4MC and Capra can help you manage timing, scheduling and traceability in a multi-core environment.  Thanks for watching!

<a class="eclipsefdn-video" href="https://www.youtube.com/watch?v=dPDuy2lXJHc"></a>
